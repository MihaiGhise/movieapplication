package com.example.MovieProject.repository;

import com.example.MovieProject.model.Client;
import com.example.MovieProject.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ReviewRepository extends JpaRepository<Review, Long> {

}