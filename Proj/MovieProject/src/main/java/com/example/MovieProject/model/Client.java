package com.example.MovieProject.model;

import com.example.MovieProject.Role;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@Table(name = "client")
public class Client {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;

    private String password;

    private String firstName;

    private String lastName;

    private String role;

    private int notificationCount = 0;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles=new HashSet<>();

    public int increment(){
        this.notificationCount++;
        return this.notificationCount;
    }

    public int decrement(){
        this.notificationCount--;
        return this.notificationCount;
    }

    public Long getId() {
        return id;
    }

    // getters and setters are not shown
}
