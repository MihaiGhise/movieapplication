package com.example.MovieProject.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.example.MovieProject.AES256;
import com.example.MovieProject.repository.ClientRepository;
import com.example.MovieProject.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class UIRestController {

    @Autowired
    private ClientRepository clientRepo;

    private int nrClients=0;

    @PostMapping("/auth")
    public Client user(@Valid @RequestBody Client client) {
        System.out.println("sunt in client");
        this.nrClients++;
        System.out.println(nrClients);
        return null;
    }

    @GetMapping("/auth/dec")
    public int cnt(){
        this.nrClients--;
        System.out.println("din decr: "+this.nrClients);
        return this.nrClients;
    }

    /*@GetMapping("/auth/cnt")
    public int getCnt(){
        System.out.println(nrClients);
        return this.nrClients;
    }*/

    @GetMapping("/auth/nrclt")
    public int nrCl(){
        System.out.println("din nr clt: "+nrClients);
        return nrClients;
    }

    @GetMapping("/auth/craaaaa")
    public void decrCl(){
        System.out.println("craaaaaa");
    }

    @GetMapping("/resource")
    public Map<String, Object> home() {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("id", UUID.randomUUID().toString());
        model.put("content", "Hello World");
        return model;
    }

    @PostMapping("/clients/register")
    public Client createClient(@Valid @RequestBody Client client) {
        System.out.println("sunt aici "+client.getEmail());
        String password = client.getPassword();
        String newPassword = AES256.encrypt(password);
        client.setPassword(newPassword);
        return clientRepo.save(client);
    }

}
