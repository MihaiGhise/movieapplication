package com.example.MovieProject.service.impl;

import com.example.MovieProject.CustomUserDetails;
import com.example.MovieProject.model.Client;
import com.example.MovieProject.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private ClientRepository clientRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Client client = clientRepo.findByEmail(username);
        if (client == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return new CustomUserDetails(client);
    }

}
