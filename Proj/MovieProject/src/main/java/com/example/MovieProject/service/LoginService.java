package com.example.MovieProject.service;

import com.example.MovieProject.dto.LoginSuccessDTO;
import com.example.MovieProject.dto.CredentialsDTO;
import com.example.MovieProject.ApiExceptionResponse;

import org.springframework.stereotype.Component;

@Component
public interface LoginService {
    LoginSuccessDTO login(CredentialsDTO dto) throws ApiExceptionResponse;
}
