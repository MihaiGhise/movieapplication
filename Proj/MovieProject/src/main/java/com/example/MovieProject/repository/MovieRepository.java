package com.example.MovieProject.repository;

import com.example.MovieProject.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long> {

    @Query("SELECT u FROM Movie u WHERE u.movieName = ?1")
    public Movie findByName(String movie_name);

    @Query("SELECT u FROM Movie u WHERE u.genre = ?1")
    public List<Movie> findByGenre(String genre);

    @Query("SELECT u FROM Movie u WHERE u.directorName = ?1")
    public List<Movie> findByDirectorName(String directorName);

}
