package com.example.MovieProject.repository;

import com.example.MovieProject.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ClientRepository extends JpaRepository<Client, Long> {
    @Query("SELECT u FROM Client u WHERE u.email = ?1")
    public Client findByEmail(String email);

    /*@Query("SELECT u FROM Client u WHERE u.email = ?1 AND u.password = ?2")
    public Client validateLogin(String email, String password);*/
}
