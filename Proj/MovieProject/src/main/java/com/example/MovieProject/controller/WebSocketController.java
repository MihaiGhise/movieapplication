package com.example.MovieProject.controller;

import com.example.MovieProject.repository.ClientRepository;
import com.example.MovieProject.Hello;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    @Autowired
    private SimpMessagingTemplate template;

    private ClientRepository cRepo;

    // Initialize Notifications

    @MessageMapping("/hello")
    @SendTo("/topic/hi")
    public Hello greeting(String mov) throws Exception {
        System.out.println("i'm in hello");
        System.out.println(mov);
        return new Hello("Hi, " + mov + "!");
    }

}
