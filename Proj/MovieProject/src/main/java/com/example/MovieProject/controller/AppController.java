package com.example.MovieProject.controller;

import com.example.MovieProject.*;
import com.example.MovieProject.model.Client;
import com.example.MovieProject.model.Movie;
import com.example.MovieProject.repository.ClientRepository;
import com.example.MovieProject.model.Reservation;
import com.example.MovieProject.model.Review;
import com.example.MovieProject.repository.MovieRepository;
import com.example.MovieProject.repository.ReservationRepository;
import com.example.MovieProject.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class AppController {

    private String theEmail;
    private List<Movie> moviesAL = new ArrayList<Movie>();
    private List<Review> reviewsg;
    private String zeEmail;
    private String zeRev;
    private int zeRating;

    @Autowired
    private ClientRepository clientRepo;

    @Autowired
    private MovieRepository movieRepo;

    @Autowired
    private ReservationRepository resRepo;

    @Autowired
    private ReviewRepository reviewRepo;

    @GetMapping("")
    public String viewHomePage() {
        return "index";
    }


    @GetMapping("/movies")
    public List<Movie> getAllMovies() {

        //System.out.println("sunt in movies");
        return movieRepo.findAll();
    }

    @DeleteMapping("/movies/myresvs/{id}")
    public Map<String, Boolean> deleteReservation(@PathVariable(value = "id") Long reservId)
            throws ResourceNotFoundException {
        System.out.println("ACUM VOI STERGE");
        Reservation reservation = resRepo.findById(reservId)
                .orElseThrow(() -> new ResourceNotFoundException("movie not found for this id :: " + reservId));

        resRepo.delete(reservation);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @PostMapping("/movies/myresvs")
    public List<Reservation> myResvs(@Valid @RequestBody String email)throws ResourceNotFoundException{
        System.out.println("Sunt an resvsvsvs");
        List<Reservation> resvs = resRepo.findAll();
        List<Reservation> resvss = new ArrayList<Reservation>();
        Client client = clientRepo.findByEmail(email);
        for(Reservation res: resvs){
            if (res.getUserEmail().equals(client.getEmail())){
                resvss.add(res);
            }
        }
        return resvss;
    }

    @PostMapping("/movies/reviews/sett")
    public List<Review> listIdFilms(@Valid @RequestBody String id1) throws ResourceNotFoundException{
        System.out.println("Sunt in listare 2");
        listReviews();
        System.out.println(this.reviewsg.get(0));
        Long id = Long.parseLong(id1);
        List<Review> reviewsc = new ArrayList<Review>();
        Movie movie = movieRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("movie not found for this id :: " + id));
        for(Review rev : this.reviewsg){
            if (rev.getMovieName().equals(movie.getMovieName())){
                reviewsc.add(rev);
                System.out.println(rev.getClientName());
            }
        }
        return reviewsc;
    }

    @GetMapping("/movies/reviews")
    public List<Review> listReviews(){
        this.reviewsg = reviewRepo.findAll();
        return this.reviewsg;
    }

    @PostMapping("/movies/reviews/createId")
    public Review revIdFilm(@Valid @RequestBody String id1) throws ResourceNotFoundException{
        System.out.println("creez Review: "+id1);
        Long id = Long.parseLong(id1);
        Movie movie = movieRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("movie not found for this id :: " + id));
        Review rev = new Review();
        rev.setMovieName(movie.getMovieName());
        rev.setClientName(this.zeEmail);
        rev.setReview(this.zeRev);
        rev.setRating(this.zeRating);
        return reviewRepo.save(rev);
        //return null;
    }

    @PostMapping("/movies/reviews/rating")
    public int revRatingFilm(@Valid @RequestBody String rating){
        System.out.println("SUNT IN RATING RATATA");
        this.zeRating = Integer.parseInt(rating);
        System.out.println(this.zeRating);
        return zeRating;
    }


    @PostMapping("/movies/reviews/createEm")
    public String revClientEmail(@Valid @RequestBody String email){
        System.out.println("SUNT IN REV CLIENT NAME");
        this.zeEmail = email;
        System.out.println(this.zeEmail);
        return zeEmail;
    }

    @PostMapping("/movies/reviews/createRee")
    public String revRev(@Valid @RequestBody String rev){
        System.out.println("SUNT IN REV REV");
        this.zeRev = rev;
        System.out.println(this.zeRev);
        return zeRev;
    }

    @PostMapping("/movies/reviews/create")
    public Review createReview(@Valid @RequestBody Review review){
        return reviewRepo.save(review);
    }

    @GetMapping("/movies/recent")
    public List<Movie> getRecentMovies() {

        //System.out.println("sunt in movies");
        for (Movie mov: moviesAL){
            System.out.println("recent mov: "+mov.getMovieName());
        }
        return moviesAL;
    }

    @PostMapping("/movies/todresv")
    public List<Movie> getTodMovResv(@Valid @RequestBody String email){
        System.out.println(email);
        List<Reservation> resa = resRepo.findAll();
        List<Movie> todMov = new ArrayList<Movie>();
        for (Reservation rr: resa){
            System.out.println(rr.getMovieName()+ ":"+rr.getUserEmail());
            Movie zeMov = movieRepo.findByName(rr.getMovieName());
            System.out.println("zeMov: "+zeMov.getMovieName());
            System.out.println("zeMail: "+rr.getUserEmail());
            String date = zeMov.getDate();
            System.out.println("Date: "+zeMov.getDate());
            if (date.equals("today") && rr.getUserEmail().equals(email)) {
                todMov.add(zeMov);
                System.out.println(zeMov.getMovieName());
            }
        }
        return todMov;
    }

    @GetMapping("/movies/allresv")
    public List<Reservation> getAllReservations() {

        //System.out.println("sunt in movies");
        return resRepo.findAll();
    }

    @PostMapping("/movies/gen")
    public List<Movie> listMoviesByGenre(@Valid @RequestBody String genre) {

        System.out.println("genre din backend: "+genre);
        return movieRepo.findByGenre(genre);
    }

    @PostMapping("/movies/direc")
    public List<Movie> listMoviesByDirectorName(@Valid @RequestBody String directorName) {

        System.out.println("genre din backend: "+directorName);
        return movieRepo.findByDirectorName(directorName);
    }

    @GetMapping("/clients")
    public List<Client> listClients() {

        List<Client> clients = clientRepo.findAll();
        List<Client> clientTrim = new ArrayList<Client>();
        for(Client c : clients){
            c.setPassword(AES256.decrypt(c.getPassword()));
            clientTrim.add(c);
        }
        return clientTrim;
    }

    @PostMapping("/movies/res")
    public Reservation movieReservation(@Valid @RequestBody Long id) throws ResourceNotFoundException {
        System.out.println("sunt aici");
        Movie movie = movieRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("movie not found for this id :: " + id));
        Reservation reservation = new Reservation();
        reservation.setMovieName(movie.getMovieName());
        reservation.setUserEmail(this.theEmail);
        return resRepo.save(reservation);
    }

    @PostMapping("/movies/resv")
    public String movieResv(@Valid @RequestBody String email) throws ResourceNotFoundException {
        System.out.println("iau mailu");
        this.theEmail = email;
        return email;
    }
    @GetMapping("/movies/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable(value = "id") Long movieId)
            throws ResourceNotFoundException {
        Movie movie = movieRepo
                .findById(movieId)
                .orElseThrow(() -> new ResourceNotFoundException("movie not found for this id :: " + movieId));
        return ResponseEntity.ok().body(movie);
    }


    @PostMapping("/movies")
    public Movie createMovie(@Valid @RequestBody Movie movie) {
        System.out.println("sunt aici"+movie.getMovieName());
        moviesAL.add(movie);
        return movieRepo.save(movie);

    }

    @PutMapping("/movies/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable(value = "id") Long movieId,
                                                   @Valid @RequestBody Movie movieDetails) throws ResourceNotFoundException {
        Movie movie = movieRepo.findById(movieId)
                .orElseThrow(() -> new ResourceNotFoundException("movie not found for this id :: " + movieId));

        movie.setMovieName(movieDetails.getMovieName());
        movie.setDirectorName(movieDetails.getDirectorName());
        movie.setTicketPrice(movieDetails.getTicketPrice());
        final Movie updatedmovie = movieRepo.save(movie);
        return ResponseEntity.ok(updatedmovie);
    }

    @DeleteMapping("/movies/{id}")
    public Map<String, Boolean> deleteMovie(@PathVariable(value = "id") Long movieId)
            throws ResourceNotFoundException {
        Movie movie = movieRepo.findById(movieId)
                .orElseThrow(() -> new ResourceNotFoundException("movie not found for this id :: " + movieId));

        movieRepo.delete(movie);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }




}