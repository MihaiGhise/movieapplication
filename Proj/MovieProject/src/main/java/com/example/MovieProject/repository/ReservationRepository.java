package com.example.MovieProject.repository;

import com.example.MovieProject.model.Client;
import com.example.MovieProject.model.Movie;
import com.example.MovieProject.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    //@Query("SELECT u FROM Reservation u WHERE u.user_email = ?1")
    //public List<Reservation> findByClientEmail(String email);
}
