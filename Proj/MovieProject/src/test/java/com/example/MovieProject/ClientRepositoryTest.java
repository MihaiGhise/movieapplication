package com.example.MovieProject;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.MovieProject.model.Client;
import com.example.MovieProject.repository.ClientRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.Rollback;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class ClientRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ClientRepository repo;
    @Test
    public void testCreateClient() {
        Client client = new Client();
        client.setEmail("gioni@gmail.com");
        client.setFirstName("Ion");
        client.setLastName("Dumitru");
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode("123456");
        client.setPassword(encodedPassword);
        Client savedClient = repo.save(client);

        Client existClient = entityManager.find(Client.class, savedClient.getId());

        assertThat(client.getEmail()).isEqualTo(existClient.getEmail());

    }
    // test methods go below
}
