import { MovieService } from '../services/movie.service';
import { Movie } from '../model/movie';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { SharedDataService } from '../services/shared-data.service';
import { Observable, Subscription } from 'rxjs';
import { Review } from '../model/review';
import { ReviewService } from '../services/review.service';
import { Reservation } from '../model/reservation';

@Component({
  selector: 'app-show-reservations',
  templateUrl: './myReservations.component.html',
  styleUrls: ['./myReservations.component.css']
})
export class MyReservationComponent implements OnInit {

  review:Review = new Review();
  submitted = false;
  id!: number;
  email!:string;
  reservations!:Observable<Reservation>;


  constructor(private route: ActivatedRoute,private movieService: MovieService,
    private router: Router, private authService: AuthService, 
    private data: SharedDataService, private reviewService: ReviewService) { }

  ngOnInit() {
    if (!this.authService.isUserLoggedin()){
      this.router.navigate(['/login']);
    }
    //this.id = this.route.snapshot.params['id'];
    this.email = this.authService.getLoggedinUser();
    this.getMyResv();
  }

  reloadData(){
    this.email = this.authService.getLoggedinUser();
    this.getMyResv();
  }

  deleteReservation(id: number) {
    console.log("COMP STERG")
    this.movieService.delReservation(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  getMyResv(){
    this.reservations=this.movieService.getMyResvs(this.email);
  }
  
}