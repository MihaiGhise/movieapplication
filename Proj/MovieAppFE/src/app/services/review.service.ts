import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Movie } from '../model/movie';
import { Review } from '../model/review';
import { Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  private baseUrl = 'http://localhost:8080/api/movies/reviews';

  constructor(@Inject(HttpClient) private http: HttpClient) { }

  
  listIdFilms(id:number): Observable<any>{
    console.log("TRIMIT DIN SERVICE AIDIUL:")
    console.log(id)
    return this.http.post(`${this.baseUrl}/sett`, id);
  }

  revRatingFilm(rating:number): Observable<any>{
    console.log("TRIMIT DIN SERVICE AIDIUL:")
    console.log(rating)
    return this.http.post(`${this.baseUrl}/rating`, rating);
  }

  revIdFilm(id:number): Observable<any>{
    console.log("TRIMIT DIN SERVICE AIDIUL:")
    console.log(id)
    return this.http.post(`${this.baseUrl}/createId`, id);
  }

  revClientEmail(email:string): Observable<any>{
    return this.http.post(`${this.baseUrl}/createEm`, email);
  }

  revRev(rev:string): Observable<any>{
    return this.http.post(`${this.baseUrl}/createRee`, rev);
  }
  
  getMovie(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createReview(review: Review): Observable<Object> {
    console.log("pas 1 ");
    console.log(`${this.baseUrl}`);
    return this.http.post(`${this.baseUrl}/create`, review);
  }


  updateReview(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteReview(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  listReviews(): Observable<any> {
    console.log("yeeeee");
    return this.http.get(`${this.baseUrl}`);
  }

}