import {Injectable} from "@angular/core";
import { Observable, Subscription } from "rxjs";
import * as SockJS from "sockjs-client";
import { Movie } from "../model/movie";
import { MovieService } from "./movie.service";
import { SharedDataService } from "./shared-data.service";

var SockJs = require("sockjs-client");
var Stomp = require("stompjs");

@Injectable({
    providedIn: 'root'
  })
export class WebSocketService{

   
     greetings: string[] = [];
     movies!: Observable<Movie[]>;
     disabled = true;
     name!: string;
     stompClient: any;
     message!:String;
     subscription!: Subscription;
   
     constructor(private movieService: MovieService, private data:SharedDataService) { }
     iinit(): void {
       this.movies = this.movieService.getRecentMovies();
       this.connect();
       this.sendName();
     }
     
   
     setConnected(connected: boolean) {
       this.disabled = !connected;
   
       if (connected) {
         this.greetings = [];
       }
     }
   
     connect() {
       const socket = new SockJS('http://localhost:8080/gkz-stomp-endpoint');
       this.stompClient = Stomp.over(socket);
   
       const _this = this;
       this.stompClient.connect({}, function (frame: string) {
         _this.setConnected(true);
         console.log('Connected: ' + frame);
   
         _this.stompClient.subscribe('/topic/hi', function (hello: { body: string; }) {
           _this.showGreeting(JSON.parse(hello.body).greeting);
         });
       });
       console.log("CONNECTED")
     }
   
     disconnect() {
       if (this.stompClient != null) {
         this.stompClient.disconnect();
       }
   
       this.setConnected(false);
       console.log('Disconnected!');
     }
   
     sendName() {
       this.stompClient.send(
         '/gkz/hello',
         {},
         JSON.stringify(this.message)
       );
     }
   
     showGreeting(message: string) {
       this.greetings.push(message);
     }
   }