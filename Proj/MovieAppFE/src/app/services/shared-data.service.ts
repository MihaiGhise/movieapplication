import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Subject } from "rxjs";

@Injectable({
    providedIn: 'root',
  })
export class SharedDataService {
    constructor(){}
    //Using any
    public editDataDetails: any = [];
    //public subject = new Subject<any>();
    private messageSource = new  BehaviorSubject<String>("default message");
    currentMessage = this.messageSource.asObservable();
    
    changeMessage(message: String) {
        console.log("IN SHARED DATA SE SCHIMBA: "+message)
        this.messageSource.next(message)
    }
}