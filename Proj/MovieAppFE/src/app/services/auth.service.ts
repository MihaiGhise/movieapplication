import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

	SESSION_KEY = 'auth_user'

	email!: String;
	password!: String;

	baseUrl:string="http://localhost:8080/api/auth";
	constructor(private http: HttpClient) {}

	authenticate(email: String, password: String) {
		console.log(email);
		console.log(password);
		return this.http.post(`http://localhost:8080/api/auth`, { 
			email : email,
			password : password
	}).pipe(map((res) => {
				this.email = email;
				this.password = password;
				this.registerInSession(email, password);
		}));
	}

	decrCl():Observable<any>{
		return this.http.get(`${this.baseUrl}/dec`)
	}

	createBasicAuthToken(email: String, password: String) {
		console.log('Basic ' + window.btoa(email + ":" + password));
		return 'Basic ' + window.btoa(email + ":" + password)
	}
	
	registerInSession(email:any, password:any) {
		sessionStorage.setItem(this.SESSION_KEY, email)
	}

	logout():Observable<any> {
		sessionStorage.removeItem(this.SESSION_KEY);
		this.email = '';
		this.password = '';
		console.log("sunt in log out");
		console.log(`${this.baseUrl}/dec`);
		return this.http.get(`${this.baseUrl}/dec`);
	}

	nrCl():Observable<any>{
		console.log("NR CL FRONTEND: ")
		return this.http.get(`${this.baseUrl}/nrclt`);
	}

	isUserLoggedin() {
		let user = sessionStorage.getItem(this.SESSION_KEY)
		if (user === null) return false
		return true
	}

	getLoggedinUser() {
		let user = sessionStorage.getItem(this.SESSION_KEY)
		if (user === null) return ''
		return user
	}

	getClient(id: number): Observable<any> {
		return this.http.get(`${this.baseUrl}/${id}`);
	  }
}