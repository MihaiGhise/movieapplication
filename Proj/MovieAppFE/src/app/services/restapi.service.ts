import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestapiService {

  constructor(private http:HttpClient) { }

login(email:string,password:string){
  const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(email + ':' + password) });
  return this.http.get("http://localhost:8080/api",{headers,responseType: 'text' as 'json'})
}

  getUsers() {
    let email='javatechie'
    let password='jt143'
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(email + ':' + password) });
   return  this.http.get("http://localhost:8080/getUsers",{headers});
  }
}