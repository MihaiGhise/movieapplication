import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Movie } from '../model/movie';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private baseUrl = 'http://localhost:8080/api/movies';

  constructor(private http: HttpClient) { }

  delReservation(id:number): Observable<any>{
    console.log("SERV STERG")
    return this.http.delete(`${this.baseUrl}/myresvs/${id}`, { responseType: 'text' });
  }

  getMyResvs(email:string): Observable<any>{
    return this.http.post(`${this.baseUrl}/myresvs`, email);
  }
  
  getMovie(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createMovie(movie: Movie): Observable<Object> {
    console.log("pas 1 ");
    console.log(movie.movieName + " din service");
    console.log(`${this.baseUrl}`);
    return this.http.post(`${this.baseUrl}`, movie);
  }

  getRecentMovies():Observable<any>{
    console.log("SOGOOR");
    return this.http.get(`${this.baseUrl}/recent`);
  }

  updateMovie(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteMovie(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  listMovies(): Observable<any> {
    console.log("yeeeee");
    return this.http.get(`${this.baseUrl}`);
  }

  getAllReservations(): Observable<any> {
    console.log("resev");
    return this.http.get(`${this.baseUrl}/allresv`);
  }

  getTodMovResv(email: String): Observable<any> {
    console.log("tod");
    return this.http.post(`${this.baseUrl}/todresv`, email);
  }

  listMoviesByGenre(genre: String): Observable<any>{
    console.log("janra");
    return this.http.post(`${this.baseUrl}/gen`, genre);
  }

  listMoviesByDirectorName(directorName: String): Observable<any>{
    console.log("derector");
    return this.http.post(`${this.baseUrl}/direc`, directorName);
  }

  movieReservation(id: number): Observable<any>{
    return this.http.post(`${this.baseUrl}/res`, id);
  }

  movieResv( email: String): Observable<any>{
    return this.http.post(`${this.baseUrl}/resv`, email);
  }

}