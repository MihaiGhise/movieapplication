  
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  baseURL:string="http://localhost:8080/login";

  constructor(private httpClient:HttpClient) { }

  login(username:string,password:string){
    return this.httpClient.post(this.baseURL,{
      username:username,
      password:password
    })
  }
}