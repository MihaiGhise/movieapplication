import { Movie } from '../model/movie';
import { Component, OnInit, Input } from '@angular/core';
import { MovieService } from '../services/movie.service';
import { MovieListComponent } from '../moviePage/movieList.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Reservation } from '../model/reservation';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {

  id!: number;
  movie!: Movie;
  creditCardInfo!: String;
  submitted = false;
  email!: String;
  error!: string;
  reservation: Reservation = new Reservation();

  constructor(private route: ActivatedRoute,private router: Router,
    private movieService: MovieService, private authService: AuthService) { }

  ngOnInit() {
    this.movie = new Movie();
    this.email = this.authService.getLoggedinUser();
    this.id = this.route.snapshot.params['id'];
    if (!this.authService.isUserLoggedin()){
      this.router.navigate(['/login']);
    }
    this.movieService.getMovie(this.id)
      .subscribe(data => {
        console.log(data)
        this.movie = data;
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['movies']);
  }

  trim(id:number){
      if (this.creditCardInfo.length === 16){
        this.movieService
    .movieResv(this.email).subscribe(data => {
      console.log(data)
      this.list();
    },
    error => console.log(error));
    this.movieService
    .movieReservation(id).subscribe(data => {
      console.log(data)
      this.reservation = new Reservation();
      //this.list();
    },
    error => console.log(error));
      }else{
      this.error = "Invalid credit card";
      console.log(this.email)
      }
  }

  onSubmit(id: number){
    this.submitted = true;
    console.log("din movie details: "+id);
    this.trim(id);
  }
}