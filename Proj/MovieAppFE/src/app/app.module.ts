import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateMovieComponent } from './createMovie/createMovie.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { MovieListComponent } from './moviePage/movieList.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UpdateMovieComponent } from './update-movie/update-movie.component';
import { LoginService } from './services/login.service';
import { LoginComponent } from './login/login.component';
import {MatList, MatListModule} from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
//import { HomeComponent } from './home/home.component';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { WatchersComponent } from './watchers/watchers.component';
import { RegisterComponent } from './register/register.component';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notificationPage/notification.component';
import { SharedDataService } from './services/shared-data.service';
import { WebSocketService } from './services/websocket.service';
import { ReviewComponent } from './review/review.component';
import { MyReservationComponent } from './myReservations/myReservations.component';
@NgModule({
  declarations: [
    AppComponent,
    CreateMovieComponent,
    MovieDetailsComponent,
    MovieListComponent,
    UpdateMovieComponent,
    LoginComponent,
    WatchersComponent,
    RegisterComponent,
    NotificationComponent,
    ReviewComponent,
    MyReservationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatListModule,
    ReactiveFormsModule,
    MatListModule,
    MatSnackBarModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatTableModule,
    CommonModule
  ],
  /*providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: HttpInterceptorService,
    multi: true,
  }],*/
  providers: [WebSocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
