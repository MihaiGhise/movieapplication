import { MovieService } from '../services/movie.service';
import { Movie } from '../model/movie';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { SharedDataService } from '../services/shared-data.service';
import { Observable, Subscription } from 'rxjs';
import { Review } from '../model/review';
import { ReviewService } from '../services/review.service';

@Component({
  selector: 'app-create-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  review:Review = new Review();
  submitted = false;
  id!: number;
  reviews!:Observable<Review>;
  rating!: number
  error!: string;


  constructor(private route: ActivatedRoute,private movieService: MovieService,
    private router: Router, private authService: AuthService, 
    private data: SharedDataService, private reviewService: ReviewService) { }

  ngOnInit() {
    if (!this.authService.isUserLoggedin()){
      this.router.navigate(['/login']);
    }
    this.id = this.route.snapshot.params['id'];
    this.reviews = this.reviewService.listIdFilms(this.id);
  }
  


  newReview(): void {
    this.submitted = false;
    this.review = new Review();
  }

  save(id: number) {
    /*this.data.changeMessage(this.review.review)
    console.log("MESAJ SCHIMBAT: "+this.review.review)
    console.log("ai di: "+this.review.id)
    console.log("imeil: "+this.review.clientName)
    this.reviewService
    .createReview(this.review).subscribe(data => {
      console.log(data)
      this.review = new Review();
    },
    error => console.log(error));*/
    console.log("REITINGUL: "+this.rating)
    if (this.rating <=5 && this.rating >0 ){
    this.reviewService
    .revClientEmail(this.authService.getLoggedinUser()).subscribe(data => {
      console.log(data)
    },
    error => console.log(error));

    this.reviewService
    .revRev(this.review.review).subscribe(data => {
      console.log(data)
    },
    error => console.log(error));

    this.reviewService
    .revRatingFilm(this.rating).subscribe(data => {
      console.log(data)
    },
    error => console.log(error));

    this.reviewService
    .revIdFilm(this.id).subscribe(data => {
      console.log(data)
    },
    error => console.log(error));
  }else{
    this.error = "Invalid rating";
    }

   }

  onSubmit() {
    this.submitted = true;
    //this.review.clientName = this.authService.getLoggedinUser();
    this.save(this.id);
  }

  gotoList() {
    this.router.navigate(['/movies']);
  }
}