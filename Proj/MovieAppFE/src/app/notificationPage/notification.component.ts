import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs';
import { Movie } from '../model/movie';
import { AuthService } from '../services/auth.service';
import { MovieService } from '../services/movie.service';
import { SharedDataService } from '../services/shared-data.service';
import { WebSocketService } from '../services/websocket.service';

var SockJS = require("sockjs-client");
var Stomp = require("stompjs");


@Component({
  selector: 'app-create-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit{

 description = 'Notifications';

  greetings: string[] = [];
  movies!: Observable<Movie[]>;
  todMovies!: Observable<Movie[]>;
  disabled = true;
  name!: string;
  stompClient: any;
  message!:String;
  subscription!: Subscription;
  
  nrCl!:Observable<number>;

  constructor(private movieService: MovieService, private data:SharedDataService,
    private wsService: WebSocketService, private authService: AuthService) { }
  ngOnInit(): void {
    this.movies = this.movieService.getRecentMovies();
    this.todMovies = this.movieService.getTodMovResv(this.authService.getLoggedinUser())
    console.log("IN NOTIF USER:"+ this.authService.getLoggedinUser())
    this.nrCl = this.authService.nrCl();
    console.log("NRCL LA INIT: ")

    this.wsService.iinit();
    
    this.wsService.sendName();
    //this.wsService.sendName();
  }
  

  /*setConnected(connected: boolean) {
    this.disabled = !connected;

    if (connected) {
      this.greetings = [];
    }
  }

  connect() {
    const socket = new SockJS('http://localhost:8080/gkz-stomp-endpoint');
    this.stompClient = Stomp.over(socket);

    const _this = this;
    this.stompClient.connect({}, function (frame: string) {
      _this.setConnected(true);
      console.log('Connected: ' + frame);

      _this.stompClient.subscribe('/topic/hi', function (hello: { body: string; }) {
        _this.showGreeting(JSON.parse(hello.body).greeting);
      });
    });
  }

  disconnect() {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }

    this.setConnected(false);
    console.log('Disconnected!');
  }

  sendName() {
    this.stompClient.send(
      '/gkz/hello',
      {},
      JSON.stringify(this.message)
    );
  }

  showGreeting(message: string) {
    this.greetings.push(message);
  }*/
}