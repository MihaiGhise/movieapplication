import { MovieDetailsComponent } from '../movie-details/movie-details.component';
import { Observable } from "rxjs";
import { MovieService } from "../services/movie.service";
import { Movie } from "../model/movie";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Reservation } from '../model/reservation';
import { WebSocketService } from '../services/websocket.service';

@Component({
  selector: "app-movie-list",
  templateUrl: "./watchers.component.html",
  styleUrls: ["./watchers.component.css"]
})
export class WatchersComponent implements OnInit {
  movies!: Observable<Movie[]>;
  gnMov!: Observable<Movie[]>;
  reservation: Reservation = new Reservation();
  email!: String;
  submitted = false;
  submitted2 = false;
  genre!: String;
  directorName!: String;
  constructor(private movieService: MovieService, private wsService: WebSocketService,
    private router: Router, private authService: AuthService) {}

  ngOnInit() {
    this.reloadData();
    this.email = this.authService.getLoggedinUser();
    if (!this.authService.isUserLoggedin()){
      this.router.navigate(['/login']);
    }
    console.log(this.email+ "tehee");
    this.wsService.iinit();
    
    this.wsService.sendName();
  }

  reloadData() {
    this.movies = this.movieService.listMovies();
  }

  deleteMovie(id: number) {
    this.movieService.deleteMovie(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  movieReservation(id:number){
    this.movieService.movieReservation(id);
  }

  doLogout() {
		this.authService.logout().subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
      error => console.log(error));
		this.router.navigateByUrl('login');
	}

  movieDetails(id: number){
    this.router.navigate(['details', id]);
  }




  newReservation(): void {
    this.submitted = false;
    this.reservation = new Reservation();
  }

  save(id:number) {
    //console.log(this.reservation.movieName);
    console.log("pas 2 ");
    this.movieService
    .movieResv(this.email).subscribe(data => {
      console.log(data)
      this.gotoList();
    },
    error => console.log(error));
    this.movieService
    .movieReservation(id).subscribe(data => {
      console.log(data)
      this.reservation = new Reservation();
      this.gotoList();
    },
    error => console.log(error));
  }

  onTrim(id: number) {
    this.submitted = true;
    this.save(id);
  }

  gotoList() {
    this.router.navigate(['/watchers']);
  }

  findOcc(){
    console.log("genre from list comp: "+this.genre);
    this.gnMov = this.movieService.listMoviesByGenre(this.genre);
  }

  findOcc2(){
    console.log("directorName from list comp: "+ this.directorName);
    this.gnMov = this.movieService.listMoviesByDirectorName(this.directorName);
  }

  onSubmit() {
    this.submitted = true;
    this.findOcc();
  }

  onSubmit2() {
    this.submitted2 = true;
    this.findOcc2();
  }

  crRev(id: number){
    this.router.navigate(['review', id]);
  }
  myResvs(){
    this.router.navigateByUrl('myreservations');
  }


}