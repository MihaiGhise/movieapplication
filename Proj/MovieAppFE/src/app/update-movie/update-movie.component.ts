import { Component, OnInit } from '@angular/core';
import { Movie } from '../model/movie';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieService } from '../services/movie.service';

@Component({
  selector: 'app-update-movie',
  templateUrl: './update-movie.component.html',
  styleUrls: ['./update-movie.component.css']
})
export class UpdateMovieComponent implements OnInit {

  id!: number;
  movie!: Movie;
  submitted = false;

  constructor(private route: ActivatedRoute,private router: Router,
    private movieService: MovieService) { }

  ngOnInit() {
    this.movie = new Movie();
    this.submitted = false;
    this.id = this.route.snapshot.params['id'];
    this.movieService.getMovie(this.id)
      .subscribe(data => {
        console.log(data)
        this.movie = data;
      }, error => console.log(error));
  }

  updateMovie() {
    this.movieService.updateMovie(this.id, this.movie)
      .subscribe(data => {
        console.log(data);
        this.movie = new Movie();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    
    this.submitted = true;
    this.updateMovie();    
  }

  gotoList() {
    this.router.navigate(['/movies']);
  }
}

