import { MovieService } from '../services/movie.service';
import { Movie } from '../model/movie';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { SharedDataService } from '../services/shared-data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-create-movie',
  templateUrl: './createMovie.component.html',
  styleUrls: ['./createMovie.component.css']
})
export class CreateMovieComponent implements OnInit {

  movie: Movie = new Movie();
  submitted = false;

  message!: String;
  subscription!: Subscription;

  constructor(private movieService: MovieService,
    private router: Router, private authService: AuthService, 
    private data: SharedDataService) { }

  ngOnInit() {
    if (!this.authService.isUserLoggedin()){
      this.router.navigate(['/login']);
    }
    this.subscription = this.data.currentMessage.subscribe(message => this.message = message)
    console.log("IN CREATEMOVIE: "+this.message)
  }
  

  newMessage() {
    
    console.log("IN NEWMESSAVE CREATEMOVIE: "+this.message)
  }

  newMovie(): void {
    this.submitted = false;
    this.movie = new Movie();
  }

  save() {
    console.log(this.movie.movieName);
    console.log("pas 2 ");
    this.data.changeMessage(this.movie.movieName)
    console.log("MESAJ SCHIMBAT: "+this.movie.movieName)
    this.movieService
    .createMovie(this.movie).subscribe(data => {
      console.log(data)
      this.movie = new Movie();
      //this.gotoList();
    },
    error => console.log(error));
    
   }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/movies']);
  }
}