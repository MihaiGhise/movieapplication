export class Review {
    id!: number;
    review!: string;
    movieName!: string
    clientName!: string;
    rating!: number;
}