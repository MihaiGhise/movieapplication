export class Movie{
    id!: number;
    movieName!: String;
    directorName!: String;
    genre!: String;
    ticketPrice!: number;
    date!: string;
}