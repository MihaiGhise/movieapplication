  
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Client } from '../model/client';
import { AuthService } from '../services/auth.service';
import { ClientService } from '../services/client.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	email: string = '';
	password : string = '';
	role : string = '';
	isLoggedin = false;

	clients!: Observable<Client[]>;
	clts2: Client[] = [];
	error!: string;

	constructor(private route: ActivatedRoute, private router: Router,
		 private authService: AuthService, private clientService: ClientService) {}

	reloadData() {
		this.clients = this.clientService.listClients();
	  }

	ngOnInit() {
		this.isLoggedin = this.authService.isUserLoggedin();
		this.reloadData();
		this.clientService.listClients().subscribe((data: Client[]) => {
			console.log(data);
			this.clts2 = data;
		  });
		if(this.isLoggedin) {
			if (this.role === "ADMIN"){
				this.router.navigateByUrl('movies');
			}else{
			this.router.navigateByUrl('watchers');
			}
		}
	}

	doRegister(){
		this.router.navigateByUrl('register');
	}

	doLogin() {
		//this.clients.forEach(element => console.log(element.toString));
		let ok = 0;
		let pos = this.clts2.length+10;
		for (let i = 0; i<this.clts2.length; i++){
			if (this.email == this.clts2[i].email && this.clts2[i].password == this.password){
				ok=1;
				pos=i;
				this.role = this.clts2[i].role;
				console.log(this.role);
			}
		}
		if(ok ===1 && this.email !== '' && this.email !== null && this.password !== '' &&
		 this.password !== null) {
			this.authService.authenticate(this.email, this.password).subscribe((result)=> {
				console.log(result);
				if (this.clts2[pos].role=="ADMIN"){
					this.router.navigate(['/movies']);
				}
				else{
					this.router.navigate(['/watchers']);
				}
			}, () => {
				this.error = 'Either invalid credentials or something went wrong';
			});
		} else {
			this.error = 'Invalid Credentials';
		}
	}
}