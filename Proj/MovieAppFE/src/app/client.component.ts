import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {ClientService} from './services/client.service'
import { Client } from './model/client';

@Component({
  selector: 'app-client',
  templateUrl: './clients.html',
  //styleUrls: ['./user.component.css']
})
export class ClientComponent implements OnInit {

  clients: Client[] = [];
  constructor(private route: ActivatedRoute,
    private router: Router, private clientService: ClientService) { }

  ngOnInit(): void {
    this.clientService.listClients().subscribe((data: Client[]) => {
      console.log(data);
      this.clients = data;
    });
  }
}