import { ClientService } from '../services/client.service';
import { Client } from '../model/client';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-create-client',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  client: Client = new Client();
  submitted = false;

  constructor(private clientService: ClientService,
    private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  newclient(): void {
    this.submitted = false;
    this.client = new Client();
  }

  save() {
    console.log("pas 2 ");
    this.clientService
    .createClient(this.client).subscribe(data => {
      console.log(data)
      this.client = new Client();
      this.gotoList();
    },
    error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/login']);
  }
}