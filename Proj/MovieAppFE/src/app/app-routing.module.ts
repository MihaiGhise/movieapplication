import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { CreateMovieComponent } from './createMovie/createMovie.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieListComponent } from './moviePage/movieList.component';
import { UpdateMovieComponent } from './update-movie/update-movie.component';
import { LoginComponent } from './login/login.component';
import { WatchersComponent } from './watchers/watchers.component';
import { RegisterComponent } from './register/register.component';
import { NotificationComponent } from './notificationPage/notification.component';
import { ReviewComponent } from './review/review.component';
import { MyReservationComponent } from './myReservations/myReservations.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {path: 'review/:id', component: ReviewComponent},
  {path: 'myreservations', component: MyReservationComponent},
  {path: 'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent },
  { path: 'watchers', component: WatchersComponent },
  { path: 'movies', component: MovieListComponent },
  { path: 'add', component: CreateMovieComponent },
  { path: 'update/:id', component: UpdateMovieComponent },
  { path: 'details/:id', component: MovieDetailsComponent },
  { path: 'notifications', component: NotificationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
